{
    "Author": "Kayland Remy",
    "Email": "Kayland.r@usask.ca",
    "About the project": "The goal of this project is to use a Deep Q-Learning Network to inform trade on a given stock market",
    "Git Clone": "https://KaylandRemy@bitbucket.org/KaylandRemy/q-learning.git",
    "Trello": "https://trello.com/b/mAln4s7l/q-learning-for-trading"
}
