# Imports
import os
import random
import numpy as np
import pandas as pd
from Enviroment import Enivroment as env ### I dont know how to fix this
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

# Cuda Configuration
#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISABLE_DEVICES"] = ""
#os.environ["CUDA_VISABLE_DEVICES"] = "1"
    
# Global Parameters
#env = Env.make()
state_size = env.observation_space.shape[0]
action_size = env.action_space.n
# batch_size is a power of 2
batch_size = 2**5 #32
n_episodes = 1001
outpur_dir = 'Output/output'
if not os.path.exists(outpur_dir):
    os.makedirs(outpur_dir)

# Agent
class DQNAgent:

    # Description:
        # Parameters: 
            # state_size: the number of states in the enviroment
            # action_size: the number of actions avaiable in the enviroment
        # Agent Constructor: 
            # Initializes the Agent
        # Returns:
            # None
    def __init__(self, state_size, action_size):
        
        # Enviroment Variables: the number of states and actions
        self.state_size = state_size
        self.action_size = action_size
            
        # Action History: 
        self.memory = deque(maxlen=2000)

        # Discount Factor: how much to discount the future reward
        self.gamma = 0.95

        # Exploration Rate: how often to try a new action
        self.epsilon = 1.0
            
        # Exploration Fecay: the rate of change from Exploration to Exploitation
        self.epsilon_decay = 0.995

        # Exploration Floor: the minimum rate of Exploration
        self.epsilon_min = 0.01

        # Learning Rate: the step size for the Stocastic Gradient-Decent Optimizer
        self.learning_rate = 0.001

        # Build Model: a private method to build the ANN
        self.model = self._build_model()

    # Description:
        # Parameters: 
            # None
        # _build_model:
            # Initializes and Compiles the ANN
        # Returns:
            # Model: the compiled ANN
    def _build_model(self):

        model = Sequential()

        model.add(Dense(24, input_dim = self.state_size, activation='relu'))

        model.add(Dense(24, activation='relu'))

        model.add(Dense(self.action_size, activation='linear'))

        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        #Model
        return model
    
    # Description:
        # Parameters:
            # state: the current state
            # action: the current action
            # reward: the reward for the action at the state
            # next_state: the state after the action is taken
            # done: has the episode ended
        # remember:
            # Appends the Parameters to the Agent's memory for use during Exploitation
        # Returns:
            # None
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    # Description:
        # Parameters:
            # state: the current state
        # act
            # Exploration: Determines whether the Agent should explore by checking 
            # if a random value is smaller than the Exploration Rate
            # Exploitation: The Agent uses the model to predict the optimal action
        # Returns:
            # Exploration: a random action
            # Exploitation: the best value of from the model's prediction
    def act(self, state):
        #Exploration
        if (np.random.rand() <= self.epsilon):
            return random.randrange(self.action_size)
        #Exploitation
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])

    # Description:
        # Parameters:
            # batch_size: The amount of samples from the Agent's Memory
            # the replay will use
        # replay
            # Training: The Agent creates a mini-batch of sampled memories
            # then loops around the mini-batch setting the reward value based
            # on wheather the sample is done or not. If the sample is done the
            # reward from the sample is the reward, otherwise the reward is set
            # to the sample's reward plus the discounted reward of the 
            # next_state. The Model then creates a prediction of the current
            # state, then the Agent sets the prediction, at the first and 
            # action's index, to the reward calculated above, and fits the 
            # model to the state and prediction
            # Exploration Rate: The Agent sets its Exploration Rate to the 
            # minimum of its current Exploration time the Decay Rate or Minimum
            # Rate 
        # Returns:
            # None
    def replay(self, batch_size):

            # Training
            minibatch = random.sample(self.memory, batch_size)

            for state, action, reward, next_state, done in minibatch:
                target = reward
                if (not done):
                    target = (reward + self.gamma * np.amax(self.model.predict(next_state)[0]))
                target_f = self.model.predict(state)
                target_f[0][action] = target

                self.model.fit(state, target_f, epochs=1, verbose=0)

            # Exploration Rate
            if (self.epsilon > self.epsilon_min):
                self.epsilon *= self.epsilon_decay

    # Description
        # Parameters:
            # name: The name of the saved Weights
        # load:
            # Loads the weights from memory
        # Returns:
            # None
    def load(self, name):
        self.model.load_weights(name)

    # Description
        # Parameters:
            # name: What to name the weights we want to save
        # save:
            # Saves the weights to memory
        # Returns:
            # None
    def save(self, name):
        self.model.save_weights(name)