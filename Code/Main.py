from Agent import Q-Agent
from Enviroment import Enivroment as env

# New Agent
agent = Q-Agent.DQNAgent(state_size, action_size)

# Interaction with enviroment
done = False

for e in range(n_episodes):
    
    # Start at the begining
    state = env.reset()
    
    # Transpose the states
    state = np.reshape(state, [1, state_size])

    # Max length of the enviroment interaction
    for time in range(5000):

        # Showing the enviroment
        # env.render()

        # Setting the first action
        action = agent.act(state)

        next_state, reward, done, _ = env.step(action)

        if (not done):
            reward = reward 
        else:
            reward = -10

        next_state = np.reshape(next_state, [1, state_size])

        agent.remember(state, action, reward, next_state, done)

        state = next_state

        if (done):
            print('episode: {}/{}, score: {}, e: {:.2}'.format(e, n_episodes, time, agent.epsilon))
            break

    if (len(agent.memory) > batch_size):
        agent.replay(batch_size)

    if (e % 50 == 0):
        agent.save(outpur_dir + 'Weights_ ' + '{:04d}'.format(e) + '.hdf5')
